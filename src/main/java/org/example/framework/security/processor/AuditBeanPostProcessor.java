package org.example.framework.security.processor;

import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.exception.TooManyConstructorsException;
import org.example.framework.server.processor.BeanPostProcessor;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class AuditBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object process(Object bean, Class<?> clazz, Object... args) {
        Class<?> clazzInterface = BeanPostProcessor.class;
        Class<?>[] clazzez = clazz.getInterfaces();
        if (clazzez.length>0) {
            Class<?> firstInterface = clazzez[0];
            if (firstInterface.equals(clazzInterface)) {
                final Enhancer enhancer = new Enhancer();
                enhancer.setSuperclass(clazz);
                enhancer.setCallback((MethodInterceptor) (obj, method, arguments, proxy) -> {
                    if (!method.isAnnotationPresent(Audit.class)) {
                        return method.invoke(bean, arguments);
                    }
                    log.debug("method invoked: {}", method.getName());
                    return method.invoke(bean, arguments);
                });
                final Constructor<?>[] constructors = clazz.getConstructors();
                if (constructors.length != 1) {
                    throw new TooManyConstructorsException();
                }
                final Constructor<?> constructor = constructors[0];
                final List<Class<?>> argTypes = Arrays.stream(constructor.getParameters()).map(Parameter::getType).collect(Collectors.toList());
                return enhancer.create(argTypes.toArray(new Class[]{}), args);
            }
            return bean;
        }
        return bean;
    }
}
