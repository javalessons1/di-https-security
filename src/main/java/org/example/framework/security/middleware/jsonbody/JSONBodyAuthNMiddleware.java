package org.example.framework.security.middleware.jsonbody;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.security.auth.principal.LoginPrincipal;
import org.example.framework.security.dto.UserRQ;
import org.example.framework.server.context.AuthRQ;
import org.example.framework.server.exception.AuthenticationException;
import org.example.framework.server.http.Request;
import org.example.framework.server.middleware.Middleware;
import org.example.framework.server.context.SecurityContext;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
public class JSONBodyAuthNMiddleware implements Middleware {
  private final Gson gson;
  private final Authenticator authenticator;

  @Override
  public void handle(final Socket socket, final Request request) {
    if (SecurityContext.getPrincipal() != null) {
      return;
    }
    try {
      final UserRQ user = gson.fromJson(new String(request.getBody(), StandardCharsets.UTF_8), UserRQ.class);

      final LoginPasswordAuthenticationToken authRequest = new LoginPasswordAuthenticationToken(user.getLogin(), user.getPassword());
      if (!authenticator.authenticate(authRequest)) {
        throw new AuthenticationException("can't authenticate");
      }
      SecurityContext.setPrincipal(new LoginPrincipal(user.getLogin()));
      SecurityContext.setAuth(
              AuthRQ.builder()
                      .login("user")
                      .role("ROLE_USER")
                      .build()
      );

    } catch (Exception e) {
      SecurityContext.clear();
    }
  }
}
