package org.example.framework.security.auth;

// TODO: Java Beans:
//  - default constructor
//  - get/set
public interface AuthenticationToken {
  Object getLogin();
  Object getCredentials();
}
