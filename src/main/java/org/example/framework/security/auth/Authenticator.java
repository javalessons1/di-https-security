package org.example.framework.security.auth;

public interface Authenticator {
  boolean authenticate(AuthenticationToken request);
}
